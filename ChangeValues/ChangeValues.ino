String errors = "Errors:\n"; //String of errors.
unsigned long startTime; //In this variable will be stored time of push start button with millis().
unsigned long lostTime = 0; //In this variable will be stored time of lost with millis().
unsigned long maxLostTime = 15000; //Max time when is robot lost.

uint8_t pinSen[8] = { A0, A1, A2, A3, A4, A5, A6, A7 }; //Array of sensor pins.

uint8_t pinSBut = 2;  //Pin of start button.
uint8_t pinLSpd = 10; //Pin for PWM of left motor.
uint8_t pinLFwd = 9;  //Pin for froward movement of left motor.
uint8_t pinLBwd = 8;  //Pin for backward movement of left motor.
uint8_t pinRFwd = 7;  //Pin for froward movement of right motor.
uint8_t pinRBwd = 6;  //Pin for backward movement of right motor.
uint8_t pinRSpd = 5;  //Pin for PWM of right motor.

int kVal = 800; //Border value between black and white.
int spd = 512;  //Max speed value.


//0; [L] 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15 [R] (0 when nothing)
double optSpd[16][2] = {
//   Optimalization for left gears.
//         Optimalization for right gears.
    { -0.5,  -0.5},
    {  0.0,   1.0},
    {(1/7),   1.0},
    {(2/7),   1.0},
    {(3/7),   1.0},
    {(4/7),   1.0},
    {(5/7),   1.0},
    {(6/7),   1.0},
    {  1.0,   1.0},
    {  1.0, (6/7)},
    {  1.0, (5/7)},
    {  1.0, (4/7)},
    {  1.0, (3/7)},
    {  1.0, (2/7)},
    {  1.0, (1/7)},
    {  1.0,   0.0}
};




void setup() {

}

void loop() {

}
