#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0])) //Function for size of array.

int kVal = 300; //(800 stul, 600 deska, 250, 300 doma) Border value between black and white.

uint8_t pinSen[8] = { A0, A1, A2, A3, A4, A5, A6, A7 }; //Array of sensor pins.
#define SENSOR_COUNT (ARRAY_SIZE(pinSen)) //Size of array of sensor pins.


inline bool Black(int pinValue) { //When black return true.
  return (pinValue > kVal);
}

bool AllBlack(bool (&link)[SENSOR_COUNT]) { //When all sensor are black return true and save resulst into link.
  bool allBlack = true;

  for (int i = 0; i < SENSOR_COUNT; ++i) {
    int value = analogRead(pinSen[i]);
    allBlack = allBlack & (link[i] = Black(value)); //C++ non-short-circuiting logical AND
  }

  return allBlack;
}

bool AllBlack() { //When all sensor are black return true.
  bool _[SENSOR_COUNT];
  return AllBlack(_);
}

void WaitUntil(bool (*conditionFunction)(), bool isNow) { //Wait until conditionFunction return true and then false.
  if(!isNow)
    while(!conditionFunction());
  while(conditionFunction());
}

String Log(String message) {
  unsigned long timestamp = millis();

  unsigned short ms = timestamp % 1000; timestamp /= 1000;
  unsigned char s = timestamp % 60; timestamp /= 60;
  unsigned char m = timestamp % 60; timestamp /= 60;
  unsigned short h = timestamp;

  char buf[32];
  sprintf(buf, "%03d:%02d:%02d.%03d: ", h, m, s, ms);

  String result { buf };

  return (result += message);
}

String BlackValues(const bool (&blackVal)[SENSOR_COUNT]) {
  String bars;

  for (int i = 0, j = 0; i < SENSOR_COUNT; ++i) {
    bars += blackVal[i] ? "#" : "_";
    bars += ' ';
  }

  int len = bars.length();
  if (len > 0)
    bars.remove(len - 1);

  return bars;
}

void setup() {
  Serial.begin(9600);
  Log("Started!");
}

void loop() {
  bool black[SENSOR_COUNT];

  delay(random(1000));

  bool allBlack = AllBlack(black);

  Log(String { "All Black = " } + allBlack + " (" + BlackValues(black) + ")");
}
