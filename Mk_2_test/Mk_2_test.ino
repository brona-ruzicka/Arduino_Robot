
int cekani_v_sec = 1;
int kaHod = 250; //800 stul, 600 deska, 250 doma

String cernabila(int input) {
  
  if (input < kaHod) {
    return "_";
  } else {
    return "█";
  }

}

void setup() {
  Serial.begin(19600);
  delay(100);
}

void loop() {

  Serial.println();

  Serial.print(cernabila(analogRead(A0)));
 
  Serial.print("  ");
  Serial.print(cernabila(analogRead(A1)));
 
  Serial.print("  ");
  Serial.print(cernabila(analogRead(A2)));
 
  Serial.print("  ");
  Serial.print(cernabila(analogRead(A3)));
 
  Serial.print("  ");
  Serial.print(cernabila(analogRead(A4)));
 
  Serial.print("  ");
  Serial.print(cernabila(analogRead(A5)));
  
  Serial.print("  ");
  Serial.print(cernabila(analogRead(A6)));
 
  Serial.print("  ");
  Serial.println(cernabila(analogRead(A7)));
  
  delay(cekani_v_sec * 1000);
}
