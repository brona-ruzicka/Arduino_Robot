#include "Arduino.h"

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0])) //Function for size of array.

String errors = ""; // Sting of errors from (statsPTime/1000) seconds time that should be printed.
String stats = ""; //String of errors in testing tah were not printed.
unsigned long mBlackTime = 0; //In this variable will be stored time of first momen of registering more black lines with millis().
unsigned long maxMBlackTime = 15000; //Max time when robot register more black lines.
unsigned long lostTime = 0; //In this variable will be stored time of lost with millis().
unsigned long maxLostTime = 15000; //Max time when is robot lost.
unsigned long lastStatsTime = 0; //In this variable will be with millis() in testing mode stored when were last time complete stats.
unsigned long statsPTime = 2500; //In this variable is stored how often should be stats completed.

uint8_t pinSen[8] = { A0, A1, A2, A3, A4, A5, A6, A7 }; //Array of sensor pins.
#define SENSOR_COUNT (ARRAY_SIZE(pinSen)) //Size of array of sensor pins.

uint8_t pinSBut = 2;  //Pin of start button.
uint8_t pinLSpd = 10; //Pin for PWM of left motor.
uint8_t pinLFwd = 9;  //Pin for froward movement of left motor.
uint8_t pinLBwd = 8;  //Pin for backward movement of left motor.
uint8_t pinRFwd = 7;  //Pin for froward movement of right motor.
uint8_t pinRBwd = 6;  //Pin for backward movement of right motor.
uint8_t pinRSpd = 5;  //Pin for PWM of right motor.

int kVal = 300; //(800 stul, 300 deska, 250, 300 doma) Threshold value between black and white.
int spd = 255;  //Max speed value.
int average = -1;

bool blackVal[SENSOR_COUNT];

//0; [L] 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; [R] (0 when nothing)
double optSpd[18][2] = {
//    Speed factor for left wheels.
//          Speed factor for right wheels.
    {  0.0,  0.0}, // 0
    {  0.5,  0.5}, // 1
    {  0.0,  1.0}, // 2
    { 0.14,  1.0}, // 3
    { 0.28,  1.0}, // 4
    { 0.42,  1.0}, // 5
    { 0.56,  1.0}, // 6
    { 0.71,  1.0}, // 7
    { 0.86,  1.0}, // 8
    {  1.0,  1.0}, // 9
    {  1.0, 0.86}, // 10
    {  1.0, 0.71}, // 11
    {  1.0, 0.56}, // 12
    {  1.0, 0.42}, // 13
    {  1.0, 0.28}, // 14
    {  1.0, 0.14}, // 15
    {  1.0,  0.0}, // 16
    { -0.5, -0.5}  // 17
};

inline bool Black(int pinValue) { //When black return true.
  return (pinValue > kVal);
}

bool AllBlack(bool (&link)[SENSOR_COUNT]) { //When all sensor are black return true and save resulst into link.
  bool allBlack = true;

  for (int i = 0; i < SENSOR_COUNT; ++i) {
    int value = analogRead(pinSen[i]);
    allBlack = allBlack & (link[i] = Black(value)); //C++ non-short-circuiting logical AND
  }

  return allBlack;
}

bool AllBlack() { //When all sensors are black return true.
  bool tmp[SENSOR_COUNT];
  return AllBlack(tmp);
}

void WaitUntil(bool (*conditionFunction)()) { //Wait until conditionFunction returns true and then false.
  while(!conditionFunction());
  while(conditionFunction());
}

void SetSpd(int lineOfArray) { //Set speed and direction.
  if (optSpd[lineOfArray][0] >= 0) {
    digitalWrite(pinLBwd, LOW); //Left wheels forward.
    digitalWrite(pinLFwd, HIGH);
  } else {
    digitalWrite(pinLFwd, LOW); //Left wheels backward.
    digitalWrite(pinLBwd, HIGH);
  }

  if (optSpd[lineOfArray][1] >= 0) {
    digitalWrite(pinRBwd, LOW); //Right wheels forward.
    digitalWrite(pinRFwd, HIGH);
  } else {
    digitalWrite(pinRFwd, LOW); //Right wheels backward.
    digitalWrite(pinRBwd, HIGH);
  }

  analogWrite(pinLSpd, (int) round(abs(optSpd[lineOfArray][0]) * spd)); //Left wheels speed set.
  analogWrite(pinRSpd, (int) round(abs(optSpd[lineOfArray][1]) * spd)); //Right wheels speed set.
}

String Log(String message) {
  unsigned long timestamp = millis();

  unsigned short ms = timestamp % 1000; timestamp /= 1000;
  unsigned char s = timestamp % 60; timestamp /= 60;
  unsigned char m = timestamp % 60; timestamp /= 60;
  unsigned short h = timestamp;

  char buf[32];
  sprintf(buf, "%03d:%02d:%02d.%03d: ", h, m, s, ms);

  String result { buf };

  return (result += message);
}

String BlackValues(const bool (&blackVal)[SENSOR_COUNT]) {
  String bars;

  for (int i = 0, j = 0; i < SENSOR_COUNT; ++i) {
    bars += blackVal[i] ? "#" : "_";
    bars += ' ';
  }

  int len = bars.length();
  if (len > 0)
    bars.remove(len - 1);

  return bars;
}


void DoStats ( bool (&blackVal)[SENSOR_COUNT], int average, bool testing) {
  lastStatsTime = millis();
  if(testing) {
    stats += Log("\n  L: " + (String)optSpd[average][0] + "\n  R: " + (String)optSpd[average][1] + "\n  Line of array: " + average + "\n");
    stats += "  " + BlackValues(blackVal);
    stats += ("\n\n" + errors + "\n");
    if(Serial) {
      Serial.print(stats);
      stats = "";
    }
    errors = "  ";
  }
}

void Race(bool testing) {
  mBlackTime = 0;
  lastStatsTime = 0;
  lostTime = 0;
  
  delay(100);

  WaitUntil([]() {return (digitalRead(pinSBut) == HIGH);}); //Wait until start button is pushed and then unpushed.
  AllBlack(blackVal);
  
  SetSpd(1); //Set speed to 50%, 50% max speed.
  
  stats += Log("\n  Started!\n\n");
  DoStats (blackVal, average, testing);

  WaitUntil(AllBlack); //Wait until all sesors say black.
  AllBlack(blackVal);
  
  stats += Log("\n  Got over line!\n\n");
  DoStats (blackVal, average, testing);

  for (; !AllBlack(blackVal);) { //Do this until all sesors say black.
    int total = 0;
    int number = 0;

    bool lastBlack = false;
    bool nowBlack;
    int lines = 0;

    for (int i = 0; i < SENSOR_COUNT; ++i) { //Do this until you did this for all sensor values.
      if ((nowBlack = blackVal[i])) { //When sensor [i] says black then set nowBlack tor true.
        total += i + 1; //Add to total i+1.
        number += 1; //Add to number 1.
      }

      if (nowBlack && !lastBlack) { //When this sensor say black and last say white.
        lines += 1; //Add to lines 1.
      }

      lastBlack = nowBlack; //Give lastBlack same value as has nowBlack.
    }

    if (number > 0) { //When number of sensors saying black is higher than 0.
      average = 2 * total / number; // Count of average * 2.
      if (lines > 1) {  //When there is more black lines than one.
        if (mBlackTime == 0) { //When lostTime is 0.
          errors += Log("More black lines than one!\n  ");
          //In testing mode add to String errors time from startTime and " More black lines than one!\n".
          mBlackTime = millis(); //Set mBlackTime to millis().
        } else if((millis() - mBlackTime) > maxMBlackTime) {
          errors += Log(String { "More black lines than one for more than " } + maxMBlackTime/1000 + " sec! Ending Race.\n  ");
          //In testing mode add to String errors time from startTime and " More black lines than one for more than (maxMBlackTime/1000) sec! Ending Race.\n".
          DoStats (blackVal, average, testing);
          return;  //End function (Race)
        }
      } else {
        mBlackTime = 0;
      }
      lostTime = 0;
    } else { //Else (when number of sensors saying black is lower than 1).
      average = 17; //Set average to 0.
      if(lostTime == 0) { //When lostTime is 0.
        errors += Log("Robot got lost!\n  ");
        //In testing mode add to String errors time from startTime and " Robot get lost!\n".
        lostTime = millis(); //Set lostTime to millis().
      } else if((millis() - lostTime) > maxLostTime) {
        errors += Log(String { "Lost for more than " } + maxLostTime/1000 + " sec! Ending Race.\n  ");
        //In testing mode add to String errors time from startTime and " Lost for more than (lostTime/1000) sec! Ending Race.\n".
        DoStats (blackVal, average, testing);
        return;  //End function (Race)
      }
    }

    SetSpd(average); //Set speed at average.
    if (((lastStatsTime == 0) || (millis() - lastStatsTime) > 2500))
      DoStats (blackVal, average, testing);
  }
  stats += Log("\n  Endline!\n\n");
  DoStats (blackVal, average, testing);

  SetSpd(9);
  delay(1500);

  SetSpd(17);
  delay(500);

  SetSpd(0);
  
  stats += Log("\n  Program ended!\n\n");
  DoStats (blackVal, average, testing);
}


void setup() {
  pinMode(pinSBut, INPUT);
  pinMode(pinLFwd, OUTPUT);
  pinMode(pinLBwd, OUTPUT);
  pinMode(pinRFwd, OUTPUT);
  pinMode(pinRBwd, OUTPUT);

  Serial.begin(19200);
  stats += Log("\n  Serial begin!\n\n");
  Serial.print(stats);
}

void loop() {
  stats = "";
  
  Race(true);
  SetSpd(0);
  
}
